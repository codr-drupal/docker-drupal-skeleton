.PHONY: assets
assets: ## Compile assets.
	@$(DOCKER_COMPOSE) exec node /bin/sh -c ' \
		yarn --cwd /app/scripts/assets install && \
		yarn --cwd /app/scripts/assets run gulp-dev'

.PHONY: assets-production
assets-production: ## Compile assets for production.
	@$(DOCKER_COMPOSE) exec node /bin/sh -c ' \
		yarn --cwd /app/scripts/assets install && \
		yarn --cwd /app/scripts/assets run gulp-prod'
