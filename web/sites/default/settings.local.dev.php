<?php

//*
//DEV
assert_options(ASSERT_ACTIVE, TRUE);
assert_options(ASSERT_EXCEPTION, TRUE);

// Change kint maxLevels setting:
if (class_exists('Kint')) {
  \Kint::$depth_limit = 4;
  \Kint::$plugins = array_merge(\Kint::$plugins, [
    '\\Kint\\Parser\\ClassMethodsPlugin',
    '\\Kint\\Parser\\ClassStaticsPlugin',
    '\\Kint\\Parser\\IteratorPlugin',
  ]);
}

error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';
$settings['cache']['bins']['render'] = 'cache.backend.null';
$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';
$settings['extension_discovery_scan_tests'] = TRUE;
$settings['rebuild_access'] = FALSE;
$settings['skip_permissions_hardening'] = TRUE;
$settings['class_loader_auto_detect'] = FALSE;
$settings['trusted_host_patterns'][] = '.*';

$config['system.logging']['error_level'] = 'verbose';
$config['system.performance']['css']['preprocess'] = FALSE;
$config['system.performance']['js']['preprocess'] = FALSE;

//$databases['secondary']['default'] = array (
//  'database' => "secondary",
//  'username' => $_ENV['DB_USER'],
//  'password' => $_ENV['DB_PASSWORD'],
//  'prefix' => '',
//  'host' => $_ENV['DB_HOST'],
//  'port' => $_ENV['DB_PORT'],
//  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
//  'driver' => 'mysql',
//);
