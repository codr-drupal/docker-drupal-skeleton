<?php

ini_set('error_reporting', E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT);

$config_directories = array();

$config['config_split.config_split.' . $_ENV['APP_ENV']]['status'] = TRUE;

$settings['hash_salt'] = $_ENV['APP_HASH_SALT'];
$settings['update_free_access'] = FALSE;
$settings['file_scan_ignore_directories'] = [
  'node_modules',
  'bower_components',
];
$settings['config_sync_directory'] = '../config/sync';
$settings['http_client_config']['force_ip_resolve'] = 'v4'; // Force IPv4 for GuzzleHTTP Client
$settings['file_private_path'] = $_ENV['APP_DIR_PRIVATE'];
$settings['trusted_host_patterns'] = explode(',', $_ENV['APP_TRUSTED_HOST_PATTERNS']);
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/default/services.yml';

//$settings['twig_sandbox_allowed_classes'] = [
//  // This is default class
//  Drupal\Core\Template\Attribute::class,
//  // Add custom class here
//  // ...
//];

$databases = [];
$databases['default']['default'] = [
  'database' => $_ENV['DB_NAME'],
  'username' => $_ENV['DB_USER'],
  'password' => $_ENV['DB_PASSWORD'],
  'prefix' => '',
  'host' => $_ENV['DB_HOST'],
  'port' => $_ENV['DB_PORT'],
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => $_ENV['DB_DRIVER'],
];

if (strtolower($_ENV['REDIS_ENABLED']) === 'on') {
  $settings['redis.connection']['interface'] = $_ENV['REDIS_CONNECTION_INTERFACE'];
  $settings['redis.connection']['host']      = $_ENV['REDIS_CONNECTION_HOST'];
  $settings['redis.connection']['base']      = $_ENV['REDIS_CONNECTION_BASE'];
  $settings['cache']['default']              = 'cache.backend.redis';
  $settings['cache_prefix'] = implode('.', [
    $_ENV['PROJECT_NAME'],
    $_ENV['BUILD_VERSION'],
    \Drupal::VERSION,
    __DIR__,
  ]);
}

$local_settings = dirname(__FILE__) . '/settings.local.php';

if (file_exists($local_settings)) {
  include $local_settings;
}
