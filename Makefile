include .env

DOCKER_COMPOSE = docker compose

include docker.mk
include drupal.mk
include node.mk
