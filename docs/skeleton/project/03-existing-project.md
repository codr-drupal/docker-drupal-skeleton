# 🚀 Launch project

## Environment preparation

We provide some example files which works for 99% of use cases.

```
make init-dev
```

This command will create `.env`, `docker-compose.override.yml` and `settings.local.php`(for dev purpose)

### .env

Check variables, and adjust as needed `PROJECT_NAME` and `PROJECT_BASE_URL` (or others)

You don't have to worry about passwords since it's just a local env.

### docker-compose.override.yml

This file is ignored in git, because it can vary from dev to dev (linux or mac, open ports, ...)

### settings.local.php

For local dev, we remove all drupal cache, and override some `$settings`.

## Turn it on

```
make up
```

All containers are launched (prefixed with `PROJECT_NAME` as set in .env)

You should view site on `http://PROJECT_BASE_URL` (as set in .env)

By default : `http://drupal.docker.localhost`

You should be redirected to `/core/install.php` script.


# First install

```
make install-drupal
```

It will drop DB and perform a site install based on existing exported config

And do not forget to build theme assets

```
# While working on assets (watching)
make assets

# Just build assets
make assets-production
```
