# ✅ Prerequisites

## Docker

Obviously

## Traefik

In order to enjoy this skeleton, you may have a local traefik (preferred method)

You can try [this traefik docker-compose](https://gitlab.com/florenttorregrosa-docker/apps/docker-traefik)

[Follow the doc](https://gitlab.com/florenttorregrosa-docker/apps/docker-traefik/-/blob/main/README.md) and you will have a self-signed ssl for `*.docker.localhost`

## Next steps

* [🏁 Start a new project](01-new-project.md)
* [🚀 Launch an existing project](03-existing-project.md)
