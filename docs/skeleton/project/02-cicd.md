# 🤖 CICD

## Gitlab

<!--

Work in progress

### Portainer

```shell
cp ./config/cicd/gitlab/.gitlab-ci.portainer.yml .gitlab-ci.yml
```

Define variables in Settings > CICD > Variables

| Variable name     | Description                    | Masked |
|-------------------|--------------------------------|--------|
| PORTAINER_WEBHOOK | Service webhook from portainer | Yes    |

Each variable should be defined for each environment defined in `.gitlab-ci.yml` (staging and production by default)
-->

### SSH

```shell
cp ./config/cicd/gitlab/.gitlab-ci.ssh.yml .gitlab-ci.yml
```

Define variables in Settings > CICD > Variables

| Variable name             | Description                                                        | Example                        | Attributes |
|---------------------------|--------------------------------------------------------------------|--------------------------------|------------|
| APP_DIR_PRIVATE           | Drupal's private folder absolute path                              |                                |            |
| APP_ENV                   | Environnement (used for config_split)                              |                                |            |
| APP_HASH_SALT             | Drupal's hash_salt                                                 |                                | Masked     |
| APP_PATH                  | Drupal's web path                                                  | ${DEPLOYMENT_PATH}/current/web | Expanded   |
| APP_TRUSTED_HOST_PATTERNS |                                                                    | '^(\\.+\\\.)?mysite\\\.com$'   |            |
| APP_URI                   |                                                                    | https://www.mysite.com/        |            |
| CI_SSH_KEY                | SSH Private key allowed to deploy                                  |                                | File       |
| DB_DRIVER                 | Drupal's database db driver                                        | mysql                          |            |
| DB_HOST                   | Drupal's database db host                                          |                                |            |
| DB_NAME                   | Drupal's database db name                                          |                                |            |
| DB_PASSWORD               | Drupal's database db password                                      |                                | Masked     |
| DB_PORT                   | Drupal's database db port                                          | 3306                           |            |
| DB_USER                   | Drupal's database db user                                          |                                |            |
| DEPLOYMENT_PATH           | Absolute path on server where released and current will be.        | /var/www/mysite                |            |
| REDIS_ENABLED             | Is redis enabled for this site ?                                   | Yes or No                      |            |
| REDIS_CONNECTION_BASE     | Redis base number (in case of shared server, don't reuse the same) | 0                              |            |
| REDIS_CONNECTION_HOST     | Redis host                                                         |                                |            |
| SERVER_HOST               | Target server where deploy will happen                             | w.x.y.z                        |            |
| SERVER_PORT               | Target server where deploy will happen                             | 22                             |            |
| SSH_USER                  | User allowed to deploy on target                                   |                                |            |

Each variable should be defined for each environment defined in `.gitlab-ci.yml` (staging and production by default)

> Edit ./scripts/make/init-cicd.sh and execute make command.
> 🛑 DO NOT COMMIT IT, it will contains sensitive data

```shell
make init-cicd
```

It will add a scheduled pipeline which will launch, once a week, at random time of the day, Security test and quality controls, via `composer audit` and `phpstan`
