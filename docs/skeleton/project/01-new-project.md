# 🏁 Project init

## Git

On your local machine, clone this repo with the target name of your choice and enter the newly created directory

```
git clone -b 10.2.x https://gitlab.com/codr-drupal/docker-drupal-skeleton.git my_awesome_project
cd my_awesome_project
```

Create your branches

```
git checkout -b main
```

Remove the remote repo and replace with your own.

```
git remote remove origin
git remote add origin git@gitlab.com:codr-my_awesome_company/my_awesome_project.git
git push -u origin main
```

Or keep the current origin as upstream for future update

```
git remote rename origin upstream
git remote add origin git@gitlab.com:codr-my_awesome_company/my_awesome_project.git
git push -u origin main
```

## Environment preparation

Edit .env.dist to match your project

```
vi .env.dist
```

Adjust as needed `PROJECT_NAME` and `PROJECT_BASE_URL` (or others)
You don't have to worry about passwords since it's just a local env.

## Boot stack

```
make init-dev
make up
```

For more info about these commands, read [Launch project](03-existing-project.md)

## Project initialization (Only the first time)

```
make init-drupal
```

This command will do the following actions :

- Runs a `drush site-install` command, followed by `drush cex`.

You can now commit your project and start working.

Do not forget to build theme assets

```
# While working on assets (watching)
make assets

# Just build assets
make assets-production
```

## Next step

People working with you, or to install project next time, use this:

* [🚀 Launch an existing project](03-existing-project.md)
