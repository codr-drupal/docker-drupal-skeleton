# 🧨 Troubleshooting

Many issues on macOs. These can be solved, but this mean only one drupal stack can be launched at any time.

## make up

On macOs, with an M1 chipset, uncomment the following lines in docker-compose.override.yml

```
pma: 
  platform: linux/x86_64

mailhog: 
  platform: linux/x86_64

mariadb: 
  platform: linux/x86_64
```

## Traefik url not found

On macOs, uncomment the following lines in docker-compose.override.yml

```
networks:
# For macOS users
  default:
    external: true
    name: traefik
```

Only one project can work at a time

## Can't edit / delete file

Some files created in the container with `drush generate` for instance, belong to internal container user.

To fix that, a make command exists, which will fix permission for custom themes and modules

```
make fix-permissions
```

## Can't browse BrowserSync

If browsing `external` (ip) doesn't work, you have to manually open ports.

Uncomment the following lines in docker-compose.override.yml

```
  node: 
    ports: 
      - "3000:3000"
```

Then, reload docker stack, restart gulp BrowserSync and navigate, this time, with `Local` (http://localhost:3000)

```
make up
make assets
```
