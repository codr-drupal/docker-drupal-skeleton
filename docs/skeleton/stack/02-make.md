# `make` commands

Since this stack os based on [wodby/docker4drupal](https://github.com/wodby/docker4drupal), all their [`make` commands](https://wodby.com/docs/1.0/stacks/drupal/local/#make-commands) are available.

Commands are separated in multiple files

## docker.mk

| Command                             | Description                                                                                        |
|-------------------------------------|----------------------------------------------------------------------------------------------------|
| `make help`                         | Print commands help.                                                                               |
| `make init-dev`                     | Init drupal files                                                                                  |
| `make down`                         | Stop containers.                                                                                   |
| `make up`                           | Start up containers.                                                                               |
| `make start`                        | Start containers without updating.                                                                 |
| `make stop`                         | Stop containers.                                                                                   |
| `make prune`                        | Remove containers and their volumes.                                                               |
| `make prune mariadb`                | Prune `mariadb` container and remove its volumes.                                                  |
| `make prune mariadb solr`           | Prune `mariadb` and `solr` containers and remove their volumes.                                    |
| `make ps`                           | List running containers.                                                                           |
| `make shell`                        | Access `php` container via shell.                                                                  |
| `make bash`                         | Run command in `php` container via shell.                                                          |
| `make composer "<command>"`         | Executes `composer` command in a specified `COMPOSER_ROOT` directory (default is `/var/www/html`). |
| `make drush "<command>"`            | Executes `drush` command in a specified `DRUPAL_ROOT` directory (default is `/var/www/html/web`).  |
| `make logs [<service> [<service>]]` | View containers logs. for instance `make logs` `make logs php` `make logs php nginx`               |

## drupal.mk

| Command                                            | Description                                                                                           |
|----------------------------------------------------|-------------------------------------------------------------------------------------------------------|
| `make init-drupal`                                 | Do a fresh install from scratch and export config (WARNING : Flush all DB and existing config export) |
| `make init-cicd`                                   | Add CI/CD variables to gitlab.com                                                                     |
| `make init-remote REMOTE=<develop\|preprod\|prod>` | Create all directory structure for Drupal on SSH server                                               |
| `make install-drupal`                              | Do a fresh install from exported config(WARNING : Flush all DB)                                       |
| `make update`                                      | Update drupal after switching branch                                                                  |
| `make update-mamp`                                 | Update drupal after switching branch on mamp instance                                                 |
| `make sync`                                        | rsync public files files from staging                                                                 |
| `make sync REMOTE=prod TARGET=<private\|files>`    | rsync files files from `$REMOTE`                                                                      |
| `make dump-remote REMOTE=prod`                     | Dump sql and download dump from `$REMOTE`                                                             |
| `make fix-permissions`                             | chown wodby:wodby                                                                                     |

## node.mk

| Command                  | Description                    |
|--------------------------|--------------------------------|
| `make assets`            | Compile assets for dev.        |
| `make assets-production` | Compile assets for production. |
