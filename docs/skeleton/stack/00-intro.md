# Docker Drupal Skeleton

This project provides a stack based on [wodby/docker4drupal](https://github.com/wodby/docker4drupal)

All their [`make` commands](https://wodby.com/docs/1.0/stacks/drupal/local/#make-commands) are available.

All their [docker images](https://github.com/wodby/docker4drupal#stack) too (except for solr wich use the official image in this skeletton)
