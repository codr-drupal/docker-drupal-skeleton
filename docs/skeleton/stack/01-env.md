# Environment management

To simplify file manipulation (avoid too many deep files to modify for each environment), this skeleton includes [drupal/dotenv](https://www.drupal.org/project/dotenv).

`.env` file is loaded and all variables are available through `$_ENV` in php.

That way, `settings.php` is versioned with `$_ENV` for database infos for instance. And on each environment, the only file to manipulate is `.env`

This file could easily be generated via CI/CD for instance.
