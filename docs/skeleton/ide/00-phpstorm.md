# ✅ PHP Storm

## PHP

![Screenshot_20240710_162359.png](images/Screenshot_20240710_162359.png)

## PHP Cli Interpreter

![Screenshot_20240710_162410.png](images/Screenshot_20240710_162410.png)

## Docker config (one time)

![Screenshot_20240710_162421.png](images/Screenshot_20240710_162421.png)

## Server && Mapping

![Screenshot_20240710_162446.png](images/Screenshot_20240710_162446.png)

## Frameworks

### Drupal

![Screenshot_20240710_162504.png](images/Screenshot_20240710_162504.png)

### Symfony

![Screenshot_20240710_162507.png](images/Screenshot_20240710_162507.png)
