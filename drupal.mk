include .env

COMPOSER_ROOT ?= /var/www/html
DRUPAL_ROOT ?= /var/www/html/web

REMOTE?=staging
TARGET?=files

## init-drupal : Do a fresh install from scratch and export config (WARNING : Flush all DB and existing config export)
.PHONY: init-drupal
init-drupal:
	@echo "Installing drupal for the first time for $(PROJECT_NAME)..."
	@$(DOCKER_COMPOSE) exec -T php bash -c "./scripts/make/init-drupal.sh"

## install-drupal : Do a fresh install from exported config(WARNING : Flush all DB)
.PHONY: install-drupal
install-drupal:
	@echo "Installing drupal from existing config for $(PROJECT_NAME)..."
	@$(DOCKER_COMPOSE) exec -T php bash -c "./scripts/make/install-drupal.sh"

## update : Update drupal after switching branch
.PHONY: update
update:
	@echo "Updating drupal $(PROJECT_NAME)..."
	@$(DOCKER_COMPOSE) exec -T php bash -c "./scripts/make/update.sh"

## update : Update drupal after switching branch
.PHONY: update-mamp
update-mamp:
	@echo "Updating drupal $(PROJECT_NAME)..."
	./scripts/make/update-mamp.sh

## fix-permissions : chown wodby:wodby
.PHONY: fix-permissions
fix-permissions:
	@echo "Fixing permissions for $(PROJECT_NAME)..."
	@$(DOCKER_COMPOSE) exec -T php bash -c "./scripts/make/fix-permissions.sh"

## init-cicd : Add CI/CD variables to gitlab.com
.PHONY: init-cicd
init-cicd:
	@echo "Init Gitlab CI/CD variables $(PROJECT_NAME)..."
	@$(DOCKER_COMPOSE) exec -T php bash -c "./scripts/make/init-cicd.sh"

## init-remote : Create all directory structure for Drupal
.PHONY: init-remote
init-remote:
	@echo "Init ${REMOTE} deploy in path ${DEPLOY_PATH} for ${PROJECT_NAME}..."
	./scripts/make/init-remote.sh ${REMOTE}

## dump-remote : Get a sql dump from remote env
.PHONY: dump-remote
dump-remote:
	@echo "Dump sql from ${REMOTE} ${PROJECT_NAME}..."
	./scripts/make/dump-remote.sh ${REMOTE}


## sync : rsync private or public files files from remote env
.PHONY: sync
sync:
	@echo "Sync ${REMOTE} files from ${TARGET}..."
	./scripts/make/sync.sh ${TARGET} ${REMOTE}

## phpstan : Launch phpStan
.PHONY: phpstan
phpstan:
	@$(DOCKER_COMPOSE) exec -T php bash -c "vendor/bin/phpstan"
