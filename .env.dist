BUILD_VERSION=dev

### ENV SETTINGS

APP_ENV=dev
APP_HASH_SALT=CHANGE_ME
APP_TRUSTED_HOST_PATTERNS='.*'
APP_PATH='/var/www/html/web'
APP_URI='https://drupal.docker.localhost'
APP_DIR_PRIVATE='/var/www/html/private'

### SITE SETTINGS

SITE_ADMIN_NAME=codr
SITE_ADMIN_MAIL=contact@codr.fr
SITE_MAIL=contact@codr.fr
SITE_PROFILE=skeleton_profile
SITE_BASE_THEME=skeleton_theme

### SSH SETTINGS

SSH_STAGING_ALIAS=project_staging
SSH_STAGING_PATH=/var/www/html
SSH_PREPROD_ALIAS=project_preprod
SSH_PREPROD_PATH=/var/www/html
SSH_PROD_ALIAS=project_prod
SSH_PROD_PATH=/var/www/html

### DB SETTINGS

DB_NAME=drupal
DB_USER=drupal
DB_PASSWORD=drupal
DB_ROOT_PASSWORD=password
DB_HOST=mariadb
DB_PORT=3306
DB_DRIVER=mysql

### REDIS SETTINGS

REDIS_ENABLED=Off
REDIS_CONNECTION_INTERFACE='PhpRedis'
REDIS_CONNECTION_HOST=redis
REDIS_CONNECTION_BASE=0

### CUSTOM SETTINGS (PROJECT SPECIFIC, API_KEY, ...)

# Can be used as $_ENV[....] in drupal context.
#
# Example:
# SOCIAL_AUTH_FACEBOOK_APP_SECRET=
# SOCIAL_AUTH_GOOGLE_CLIENT_SECRET=

### DOCKER SETTINGS

PROJECT_NAME=my_drupal_project
PROJECT_BASE_URL=drupal.docker.localhost

### GITLAB SETTINGS

PROJECT_ID=123456
PRIVATE_TOKEN=find_me_on_gitlab

### --- MARIADB ----

MARIADB_TAG=10.9-3.26.5

### --- PHP ----

# Linux (uid 1000 gid 1000)

PHP_TAG=8.2-dev-4.48.0
#Can't update this while curl < 8.6.0

# macOS (uid 501 gid 20)

#PHP_TAG=8.1-dev-macos-4.44.1

### --- NGINX ----

NGINX_TAG=1.25-5.34.2
NGINX_VHOST_PRESET=drupal10

### --- SOLR ---

# SOLR_TAG=8.4-slim
SOLR_TAG=8-4.18.2
SOLR_CONFIG_SET="search_api_solr_4.1.6"

### --- REDIS ---

REDIS_TAG=7-4.1.0

### --- VARNISH ---

VARNISH_TAG=6.0-4.14.0

### --- NODE ---

NODE_TAG=18-alpine
