////////////////////////////////////////////////////////////////////////////////
// Per project variables.
////////////////////////////////////////////////////////////////////////////////

const themeName = "theme_name";

////////////////////////////////////////////////////////////////////////////////
// Gulp initialization.
////////////////////////////////////////////////////////////////////////////////

const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const mode = require('gulp-mode')({
  modes: ['production', 'development'],
  default: 'development',
  verbose: false
});
const sass = require('gulp-dart-sass');
const sassGlob = require('gulp-sass-glob');
const sourcemaps = require('gulp-sourcemaps');
const browserSync = require('browser-sync').create();

////////////////////////////////////////////////////////////////////////////////
// Options and variables.
////////////////////////////////////////////////////////////////////////////////

const srcCleanOptions = {
  read: false
};
const cleanOptions = {
  force: true
};
const imageMinSvgoOptions = {
  plugins: [
    { removeUselessDefs: false },
    { cleanupIDs: false},
    { removeViewBox: false}
  ]
};
const sassOptions = {
  outputStyle: (mode.production() ? 'compressed' : 'expanded')
};
const browsersyncOptions = {
  open: false,
  proxy: {
    target: 'http://nginx'
  },
  ui: false,
};

const customModulesPath = '../../web/modules/custom';
const customThemesPath = '../../web/themes/custom';
const projectThemePath = customThemesPath + '/' + themeName;

const paths = {
  myproject_theme: {
    styles: {
      src: projectThemePath + '/assets/scss/**/*.{scss,sass}',
      dest: projectThemePath + '/assets/css'
    },
    patternsStyles: {
      src: projectThemePath + '/templates/patterns/**/styles/*.{scss,sass}'
    },
    images: {
      clean: projectThemePath + '/assets/images/optimized/**/*.{png,jpg,gif,svg}',
      src: projectThemePath + '/assets/images/source/**/*.{png,jpg,gif,svg}',
      dest: projectThemePath + '/assets/images/optimized'
    }
  },
  myproject_modules: {
    styles: {
      src: customModulesPath + '/**/assets/styles/**/*.{scss,sass}',
    },
    patternsStyles: {
      src: customModulesPath + '/**/templates/patterns/**/styles/*.{scss,sass}'
    },
    layoutsStyles: {
      src: customModulesPath + '/**/layouts/**/*.{scss,sass}'
    }
  }
};

////////////////////////////////////////////////////////////////////////////////
// Task definitions.
////////////////////////////////////////////////////////////////////////////////

// Theme tasks
gulp.task('styles_theme', () => compileSass(paths['myproject_theme'].styles));
gulp.task('pattern_styles_theme', () => compileSass(paths['myproject_theme'].patternsStyles));
gulp.task('clean_images_theme', () => themeCleanImages('myproject_theme'));
gulp.task('build_images_theme', () => themeBuildImages('myproject_theme'));

gulp.task('images', gulp.series(
  'clean_images_theme',
  'build_images_theme'
));

// Custom modules tasks.
gulp.task('styles_modules', () => compileSass(paths['myproject_modules'].styles));
gulp.task('pattern_styles_modules', () => compileSass(paths['myproject_modules'].patternsStyles));
gulp.task('layouts_styles_modules', () => compileSass(paths['myproject_modules'].layoutsStyles));

// All tasks in parallel
gulp.task('all', gulp.parallel(
  'images',
  'styles_theme',
  'pattern_styles_theme',
  'styles_modules',
  'pattern_styles_modules',
  'layouts_styles_modules',
))

// Watch.
gulp.task('watch', function (done) {
  if (mode.production()) {
    return done();
  }

  browserSync.init(browsersyncOptions);

  gulp.watch(paths['myproject_theme'].styles.src, gulp.series('all'));
  gulp.watch(paths['myproject_theme'].patternsStyles.src, gulp.series('all'));
  gulp.watch(paths['myproject_theme'].images.src, gulp.series(
    'images',
    'styles_theme',
    'pattern_styles_theme'
  ));
  gulp.watch(paths['myproject_modules'].styles.src, gulp.series('all'));
  gulp.watch(paths['myproject_modules'].patternsStyles.src, gulp.series('all'));
  gulp.watch(paths['myproject_modules'].layoutsStyles.src, gulp.series('all'));
});

// Default task.
gulp.task('default', gulp.series(
  'all',
  'watch'
));

////////////////////////////////////////////////////////////////////////////////
// Functions.
////////////////////////////////////////////////////////////////////////////////

// Sass.
function compileSass(dir) {
  return gulp.src(dir.src)
    .pipe((mode.development(sourcemaps.init({}))))
    .pipe(sassGlob())
    .pipe((mode.production(sass(sassOptions))))
    .pipe((mode.development(sass(sassOptions).on('error', sass.logError))))
    .pipe(autoprefixer())
    .pipe((mode.development(sourcemaps.write('.'))))
    .pipe(gulp.dest(dir.dest ? dir.dest : (file) => file.base))
    .pipe((mode.development(browserSync.stream())));
}

// Images.
function themeCleanImages(themePathKey) {
  return gulp.src(paths[themePathKey].images.clean, srcCleanOptions)
    .pipe(clean(cleanOptions));
}
function themeBuildImages(themePathKey) {
  return gulp.src(paths[themePathKey].images.src)
    .pipe(imagemin([
      imagemin.svgo(imageMinSvgoOptions)
    ]))
    .pipe(gulp.dest(paths[themePathKey].images.dest))
    .pipe((mode.development(browserSync.stream())));
}
