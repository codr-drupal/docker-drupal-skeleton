#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Check number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage" >&2
    echo "make init-remote REMOTE=preprod" >&2
    exit 2;
fi

# check env
ALLOWED_ENVS=("develop" "preprod" "prod")

if ! [[ ${ALLOWED_ENVS[@]} =~ ${1} ]]
then
  echo "Invalid env" >&2
  exit 2;
fi

# get env
SYNC_ENV=$(echo $1 | tr '[:lower:]' '[:upper:]')

# get dynamic variables names
SYNC_ALIAS_VAR=SSH_${SYNC_ENV}_ALIAS
SYNC_PATH_VAR=SSH_${SYNC_ENV}_PATH

# get dynamic variables values
SYNC_ALIAS=${!SYNC_ALIAS_VAR}
SYNC_PATH=${!SYNC_PATH_VAR}

# Init some vars
NO_BUILD=true
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/gitlab/init.sh

#Override $SSH_ARGS
SSH_ARGS=${SYNC_ALIAS}

#check there is no trailing "/" in ${!SYNC_PATH}
if [[ ${SYNC_PATH:0-1} = "/" ]]
then
  echo "sync_path must not containe trailing slash" >&2
  exit 2;
fi

# Create directory tree
DIRECTORIES=(shared/private shared/public backups releases maintenance/web)

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Deploy directory ${COLOR_LIGHT_BLUE}'${SYNC_PATH}'${COLOR_NC}"

for DIRECTORY in "${DIRECTORIES[@]}"
do
  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create directory ${COLOR_LIGHT_BLUE}'${DIRECTORY}'${COLOR_NC}"
  $SSH $SSH_ARGS "mkdir -p ${SYNC_PATH}/${DIRECTORY}"
done

# Upload maintenante page
echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Upload maintenance page.${COLOR_NC}"
scp $(dirname "${BASH_SOURCE[0]}")/index.php ${SYNC_ALIAS}:${SYNC_PATH}/maintenance/web

# Symlink freom Maintenance to Current
echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create symlink to maintenance page.${COLOR_NC}"
$SSH $SSH_ARGS "ln -s ${SYNC_PATH}/maintenance ${SYNC_PATH}/current"
