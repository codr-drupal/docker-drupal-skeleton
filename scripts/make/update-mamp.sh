#!/usr/bin/env bash

# Perform update
composer install
vendor/bin/drush @default.alias updatedb --no-cache-clear -y
vendor/bin/drush @default.alias cache:rebuild -y
vendor/bin/drush @default.alias config:import -y
vendor/bin/drush @default.alias config:import -y
vendor/bin/drush @default.alias locale:check -y
vendor/bin/drush @default.alias locale:update -y
vendor/bin/drush @default.alias cache:rebuild -y
vendor/bin/drush @default.alias deploy:hook -y
