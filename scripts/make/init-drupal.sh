#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Install drupal for the first time
echo "Perform initial install"
# Nuke and Composer
rm -fr vendor web/core web/{profiles,modules,themes}/{contrib,custom} composer.lock
composer install

# Actual installation
drush @default.alias sql-drop -y
drush @default.alias site-install ${SITE_PROFILE} -y \
  --account-name="${SITE_ADMIN_NAME}" \
  --account-mail="${SITE_ADMIN_MAIL}" \
  --site-name="${PROJECT_NAME}" \
  --site-mail="${SITE_MAIL}"

#Make some directories and fix permissions
sudo chgrp -R www-data web/sites/default/files
sudo chgrp -R www-data private
sudo chmod -R g+w private

# Theme
echo "Generating and installing custom theme"
mkdir -p web/themes/custom
drush @default.alias generate theme -y \
  --destination=themes/custom \
  --answer=${PROJECT_NAME} \
  --answer=${PROJECT_NAME,,} \
  --answer=${SITE_BASE_THEME} \
  --answer="${PROJECT_NAME} Theme" \
  --answer=Custom \
  --answer=no \
  --answer=no

# Move js and css into assets directory
mkdir web/themes/custom/${PROJECT_NAME,,}/assets
mv web/themes/custom/${PROJECT_NAME,,}/js web/themes/custom/${PROJECT_NAME,,}/assets/js
mv web/themes/custom/${PROJECT_NAME,,}/css web/themes/custom/${PROJECT_NAME,,}/assets/scss
mv web/themes/custom/${PROJECT_NAME,,}/images web/themes/custom/${PROJECT_NAME,,}/assets/images

# Rename css as scss
for cssFile in web/themes/custom/${PROJECT_NAME,,}/assets/scss/*/*.css; do
    sassFile=`echo $cssFile | sed 's/.css$/.scss/'`;
    mv $cssFile $sassFile;
done

# Activate and set theme by default
drush @default.alias then ${PROJECT_NAME,,} -y
drush @default.alias cset system.theme default ${PROJECT_NAME,,} -y

# Set theme name in gulp file
sed -i 's/const themeName = "theme_name";/const themeName = "'${PROJECT_NAME,,}'";/g' scripts/assets/gulpfile.js

# Update libraries destinations
sed -E -i 's/js\/(\w*).js/assets\/js\/\1.js/g' web/themes/custom/${PROJECT_NAME,,}/${PROJECT_NAME,,}.libraries.yml
sed -E -i 's/css\/(\w*)\/(\w*).css/assets\/css\/\1\/\2.css/g' web/themes/custom/${PROJECT_NAME,,}/${PROJECT_NAME,,}.libraries.yml

# Module
echo "Generating and installing custom core module"
drush @default.alias generate module -y \
  --destination=modules/custom \
  --answer="${PROJECT_NAME} Core" \
  --answer="${PROJECT_NAME,,}_core" \
  --answer="Core module for ${PROJECT_NAME,,}" \
  --answer=Custom \
  --answer="" \
  --answer=yes \
  --answer=no \
  --answer=no

# Activate module
drush @default.alias en "${PROJECT_NAME,,}_core" -y

# Config
echo "Exporting config"
drush @default.alias cex -y

# Done
echo "Installation completed"
drush @default.alias uli
