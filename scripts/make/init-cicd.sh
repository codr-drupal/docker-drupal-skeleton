#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# CI/CD variables (Adjust depending on your project servers)

## All env

### Need change
GITLAB_HOST='gitlab.com'
APP_HASH_SALT='CHANGE_ME'
CI_SSH_KEY='UPDATE_ME_ONLINE'
SERVER_HOST='x.y.z.0'
SSH_USER='www-data'

### Should not change
APP_DIR_PRIVATE='${DEPLOYMENT_PATH}/shared/private'
APP_PATH='${DEPLOYMENT_PATH}/current/web'
DB_DRIVER='mysql'
DB_HOST='localhost'
DB_PORT='3306'
REDIS_ENABLED='Off'
REDIS_CONNECTION_BASE='0'
REDIS_CONNECTION_HOST='localhost'
SERVER_PORT='22'

## Develop
DEVELOP_DEPLOYMENT_PATH='/var/www/site'
DEVELOP_APP_TRUSTED_HOST_PATTERNS='^develop\\\.site\\\.com$'
DEVELOP_APP_URI='https://develop.site.com'
DEVELOP_DB_NAME='drupal'
DEVELOP_DB_USER='drupal'
DEVELOP_DB_PASSWORD='password'
DEVELOP_APP_ENV='develop'

## Preprod
PREPROD_DEPLOYMENT_PATH='/var/www/site'
PREPROD_APP_TRUSTED_HOST_PATTERNS='^preprod\\\.site\\\.com$'
PREPROD_APP_URI='https://preprod.site.com'
PREPROD_DB_NAME='drupal'
PREPROD_DB_USER='drupal'
PREPROD_DB_PASSWORD='password'
PREPROD_APP_ENV='preprod'

## Prod
PROD_DEPLOYMENT_PATH='/var/www/site'
PROD_APP_TRUSTED_HOST_PATTERNS='^www\\\.site\\\.com$'
PROD_APP_URI='https://www.site.com'
PROD_DB_NAME='drupal'
PROD_DB_USER='drupal'
PROD_DB_PASSWORD='password'
PROD_APP_ENV='prod'

# CI/CD Variables groups
AVAILABLE_ENVS=(
  DEVELOP
  PREPROD
  PROD
)
VAR_ALL=(APP_DIR_PRIVATE APP_HASH_SALT APP_PATH CI_SSH_KEY DB_DRIVER DB_HOST DB_PORT REDIS_ENABLED REDIS_CONNECTION_BASE REDIS_CONNECTION_HOST SERVER_HOST SERVER_PORT SSH_USER)
VAR_ENV=(APP_ENV APP_TRUSTED_HOST_PATTERNS APP_URI DB_NAME DB_PASSWORD DB_USER DEPLOYMENT_PATH)

# Variables options
VAR_PROTECTED=(APP_HASH_SALT DB_PASSWORD)
VAR_FILE=(CI_SSH_KEY)
VAR_EXPANDED=(APP_DIR_PRIVATE APP_PATH)

# Helper
create_var () {
  echo "Create variable for env "${3}": ${1} -> ${2}"

  # $1 = key
  # $2 = value
  # $3 = environment

  arg_names=("key" "value" "variable_type" "raw" "masked")
  arg_values=($1 $2)

  if [[ ${VAR_FILE[@]} =~ $1 ]]
  then
    arg_values+=("file")
  else
    arg_values+=("env_var")
  fi

  if [[ ${VAR_EXPANDED[@]} =~ $1 ]]
  then
    arg_values+=("false")
  else
    arg_values+=("true")
  fi

  if [[ ${VAR_MASKED[@]} =~ $1 ]]
  then
    arg_values+=("true")
  else
    arg_values+=("false")
  fi

  # If env is * (all env) don't add it, or it fails
  if [[ "*" != $3 ]]
  then
    arg_names+=("environment_scope")
    arg_values+=("${3}")
  fi

  form=""
  for i in "${!arg_names[@]}"
  do
    form="${arg_names[$i]}=${arg_values[$i]}&${form}"
  done

  echo ">> ${form}"

  curl --silent --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
       "https://${GITLAB_HOST}/api/v4/projects/${PROJECT_ID}/variables" \
       -d "${form}"

  echo ""
  echo ""
}

# Script
echo "Common vars: "
echo ""
for VAR in "${VAR_ALL[@]}"
do
  create_var "${VAR}" "${!VAR}" "*"
done
echo ""

echo "Env specific vars"
echo ""
for ENV in "${AVAILABLE_ENVS[@]}"
do
  echo "${ENV} vars"
  echo ""
  for VAR in "${VAR_ENV[@]}"
  do
    var_name=${ENV}_${VAR}
    create_var "${VAR}" "${!var_name}" "${ENV,,}"
  done
  echo ""
done

# Create scheduled pipeline
day=$(($RANDOM % 6))
hour=$((1 + $RANDOM % 23))
minute=$((1 + $RANDOM % 59))
CRON="${minute} ${hour} * * ${day}"

curl  --silent --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" \
     --form description="Scheduled Audit" --form ref="develop" --form cron="${CRON}" --form cron_timezone="Europe/Paris" \
     --form active="true" "https://${GITLAB_HOST}/api/v4/projects/${PROJECT_ID}/pipeline_schedules"
