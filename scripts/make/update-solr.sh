#!/usr/bin/env bash

### TO MOVE IN A SEPARATE FILE
###ENV INTEGRATION

# Load variables from target environment example env file.
environment_filename=.env
set -o allexport
source "$(dirname "${BASH_SOURCE[0]}")"/../"${environment_filename}"
set +o allexport

###
SOLR_INDEXES=(myapp)
SOLR_SERVERS=(solr)

usage() {
  printf "${BASH_SOURCE[0]} ${COLOR_LIGHT_BLUE}<index>${COLOR_NC}\n"
  printf "Available indexes are: ${COLOR_LIGHT_BLUE}%s${COLOR_NC}\n" "${SOLR_INDEXES[*]}"
}

# Check that the argument is there.
if [ -z "$1" ]; then
  echo -e "${COLOR_RED}Selected index is missing.${COLOR_NC}"
  usage
  exit 1
fi

SELECTED_INDEX=$1

#Check if index is available
if [[ ! " ${SOLR_INDEXES[*]} " =~ " ${SELECTED_INDEX} " ]]; then
  echo -e "${COLOR_RED}Selected index is not available.${COLOR_NC}"
  usage
  exit 1
fi

#Select server corresponding to index
for i in "${!SOLR_INDEXES[@]}"; do
   if [[ "${SOLR_INDEXES[$i]}" = "${SELECTED_INDEX}" ]]; then
     SELECTED_SERVER=${SOLR_SERVERS[$i]}
   fi
done

####
echo -e "${COLOR_GREEN}Exporting SOLR config from Drupal${COLOR_NC}"

make drush "search-api-solr:get-server-config ${SELECTED_SERVER} /var/www/html/dump/${SELECTED_INDEX}.zip"

####
echo -e "${COLOR_GREEN}Updating SOLR config${COLOR_NC}"

rm -fr ./config/solr/${SELECTED_INDEX}/conf/*
unzip -o ./dump/${SELECTED_INDEX}.zip -d ./config/solr/${SELECTED_INDEX}/conf
rm ./dump/${SELECTED_INDEX}.zip

####
echo -e "${COLOR_GREEN}Restarting SOLR container${COLOR_NC}"

docker compose stop solr
docker compose rm -f solr
docker compose up -d solr

####
echo -e "${COLOR_GREEN}Waiting container for readyness${COLOR_NC}"
sleep 6

####
echo -e "${COLOR_GREEN}Reindexing content${COLOR_NC}"

#Loop through all indexes
for i in "${!SOLR_INDEXES[@]}"; do
  make drush "search-api:clear ${SOLR_INDEXES[$i]}"
done

make drush "search-api:index"
