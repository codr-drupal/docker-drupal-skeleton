#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Check number of arguments
if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters" >&2
    exit 2;
fi

SYNC_SOURCE=$1

#$SYNC_SOURCE must be "files" or "private"
ALLOWED_SOURCES=("files" "private")

if ! [[ ${ALLOWED_SOURCES[@]} =~ ${SYNC_SOURCE} ]]
then
  echo "Invalid sync source" >&2
  exit 2;
fi

# check env
ALLOWED_ENVS=("staging" "preprod" "prod")

if ! [[ ${ALLOWED_ENVS[@]} =~ ${2} ]]
then
  echo "Invalid env" >&2
  exit 2;
fi

#if source=private, target=./
SYNC_TARGET=./

#if source=files, target=./web/sites/default/
if [[ ${SYNC_SOURCE} = "files" ]]
then
  SYNC_TARGET=./web/sites/default/
fi

#get correct ssh_alias ans ssh_path
SYNC_ENV=$(echo $2 | tr '[:lower:]' '[:upper:]')
SYNC_ALIAS_VAR=SSH_${SYNC_ENV}_ALIAS
SYNC_PATH_VAR=SSH_${SYNC_ENV}_PATH

SYNC_ALIAS=${!SYNC_ALIAS_VAR}
SYNC_PATH=${!SYNC_PATH_VAR}

#check there is no trailing "/" in ${!SYNC_PATH}
if [[ ${SYNC_PATH:0-1} = "/" ]]
then
  echo "sync_path must not containe trailing slash" >&2
  exit 2;
fi

#echo ${SYNC_ALIAS}:${SYNC_PATH}/shared/${SYNC_SOURCE} ./${SYNC_TARGET}
rsync -vcrtzh --delete-after ${SYNC_ALIAS}:${SYNC_PATH}/shared/${SYNC_SOURCE} ./${SYNC_TARGET}
