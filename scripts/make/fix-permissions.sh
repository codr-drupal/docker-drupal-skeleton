#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Perform permission fix
sudo chown wodby:wodby -R web/{themes,modules}/custom
