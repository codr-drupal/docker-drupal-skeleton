#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Perform re-install install
composer install

drush @default.alias sql-drop -y
drush @default.alias site-install ${SITE_PROFILE} --site-name="${PROJECT_NAME}" --existing-config -y

echo "Installation completed"
drush @default.alias uli
