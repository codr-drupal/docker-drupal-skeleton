#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Check number of arguments
if [ "$#" -ne 1 ]; then
    echo "Usage" >&2
    echo "make dump-remote REMOTE=preprod" >&2
    exit 2;
fi

# check env
ALLOWED_ENVS=("develop" "preprod" "prod")

if ! [[ ${ALLOWED_ENVS[@]} =~ ${1} ]]
then
  echo "Invalid env" >&2
  exit 2;
fi

# get env
SYNC_ENV=$(echo $1 | tr '[:lower:]' '[:upper:]')

# get dynamic variables names
SYNC_ALIAS_VAR=SSH_${SYNC_ENV}_ALIAS
SYNC_PATH_VAR=SSH_${SYNC_ENV}_PATH

# get dynamic variables values
SYNC_ALIAS=${!SYNC_ALIAS_VAR}
SYNC_PATH=${!SYNC_PATH_VAR}

# Init some vars
NO_BUILD=true
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/gitlab/init.sh

#Override $SSH_ARGS
SSH_ARGS=${SYNC_ALIAS}

#check there is no trailing "/" in ${!SYNC_PATH}
if [[ ${SYNC_PATH:0-1} = "/" ]]
then
  echo "sync_path must not containe trailing slash" >&2
  exit 2;
fi

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Dump DB${COLOR_NC}"
$SSH $SSH_ARGS "cd ${SYNC_PATH}/current && vendor/bin/drush sql-dump --gzip --result-file ${SYNC_PATH}/shared/private/${1}-${CURRENT_DATE}.sql"
scp $SSH_ARGS:${SYNC_PATH}/shared/private/${1}-${CURRENT_DATE}.sql.gz ./private
