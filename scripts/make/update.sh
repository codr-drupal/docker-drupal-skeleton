#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Perform update
composer install
drush @default.alias updatedb --no-cache-clear -y
drush @default.alias cache:rebuild -y
drush @default.alias config:import -y
drush @default.alias config:import -y
drush @default.alias locale:check -y
drush @default.alias locale:update -y
drush @default.alias cache:rebuild -y
drush @default.alias deploy:hook -y

echo "Installation completed"
drush @default.alias uli
