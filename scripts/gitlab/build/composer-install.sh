#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Perform composer install for dev
composer install \
  --ignore-platform-reqs \
  --no-interaction \
  --no-progress \
  --working-dir="${BUILD_PATH}"
