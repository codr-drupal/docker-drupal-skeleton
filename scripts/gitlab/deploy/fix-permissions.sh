#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

# If many server: Do on each servers
echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions to project sources.${COLOR_NC}"
$SSH $SSH_ARGS "chmod 0775 -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}"
$SSH $SSH_ARGS "chown ${PROJECT_USER}:${PROJECT_GROUP} -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set execution mode to scripts.${COLOR_NC}"
$SSH $SSH_ARGS "chmod u+x,g+x -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/scripts"
$SSH $SSH_ARGS "chmod u+x,g+x -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/vendor/bin"

#echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions for contrib translations folder.${COLOR_NC}"
#$SSH $SSH_ARGS "chmod g+w -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/app/translations/contrib"
#$SSH $SSH_ARGS "chown ${PROJECT_USER}:${WEBSERVER_GROUP} -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/app/translations/contrib"

#echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions for sites.php.${COLOR_NC}"
#$SSH $SSH_ARGS "chmod 0644 ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/conf/drupal/sites.php"
#$SSH $SSH_ARGS "chmod 0444 ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/app/sites/sites.php"
#$SSH $SSH_ARGS "chown ${PROJECT_USER}:${PROJECT_GROUP} ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/conf/drupal/sites.php"
#$SSH $SSH_ARGS "chown ${PROJECT_USER}:${PROJECT_GROUP} ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/app/sites/sites.php"

# If multi site, On each site
FOLDER_NAME="default"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions for configuration folders.${COLOR_NC}"
$SSH $SSH_ARGS "chmod 0775 -R ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/config/${!FOLDER_NAME}"

#echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: ${DRUPAL_SITE}: Set permissions for settings files.${COLOR_NC}"
#$SSH $SSH_ARGS "chmod 0644 ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/conf/drupal/${!FOLDER_NAME}/services.yml"
#$SSH $SSH_ARGS "chmod 0644 ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/conf/drupal/${!FOLDER_NAME}/settings.local.php"
#$SSH $SSH_ARGS "chmod 0444 ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/app/sites/${!FOLDER_NAME}/settings.php"
#$SSH $SSH_ARGS "chown ${PROJECT_USER}:${PROJECT_GROUP} ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/conf/drupal/${!FOLDER_NAME}/services.yml"
#$SSH $SSH_ARGS "chown ${PROJECT_USER}:${PROJECT_GROUP} ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/conf/drupal/${!FOLDER_NAME}/settings.local.php"
#$SSH $SSH_ARGS "chown ${PROJECT_USER}:${PROJECT_GROUP} ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/app/sites/${!FOLDER_NAME}/settings.php"

##

# If many servers: Only on main
# If multi site, On each site
echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions for public files folder.${COLOR_NC}"
$SSH $SSH_ARGS "chmod g+w -R ${DEPLOYMENT_PATH}/shared/files"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions for contrib translations folder.${COLOR_NC}"
$SSH $SSH_ARGS "chmod g+w -R ${DEPLOYMENT_PATH}/shared/files/translations"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Set permissions for private files folder.${COLOR_NC}"
$SSH $SSH_ARGS "chmod g+w -R ${DEPLOYMENT_PATH}/shared/private"
