#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

# on server, deploy the archive, make symlinks, etc.
echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create folders architecture in case it does not exist yet.${COLOR_NC}"
$SSH $SSH_ARGS "mkdir -p ${DEPLOYMENT_PATH}/releases"
$SSH $SSH_ARGS "mkdir -p ${DEPLOYMENT_PATH}/shared"

# Check if the release already exists on the server.
if ! $SSH $SSH_ARGS "test -e ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}"; then
  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Deploy archive.${COLOR_NC}"
  scp -P ${SERVER_PORT} "${ARTIFACT_PATH}"/"${BUILD_VERSION}.tar.gz" "${SSH_USER}"@"${SERVER_HOST}:${DEPLOYMENT_PATH}/releases/"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Extract archive.${COLOR_NC}"
  $SSH $SSH_ARGS "tar -xzf /${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}.tar.gz -C ${DEPLOYMENT_PATH}/releases"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Remove archive.${COLOR_NC}"
  $SSH $SSH_ARGS "rm /${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}.tar.gz"

else
  echo -e "${COLOR_BROWN_ORANGE}Server ${SERVER_HOST}: Release folder already exists.${COLOR_NC}"
fi

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create shared public files folder in case it is the first deployment.${COLOR_NC}"
$SSH $SSH_ARGS "mkdir -p ${DEPLOYMENT_PATH}/shared/files"
$SSH $SSH_ARGS "mkdir -p ${DEPLOYMENT_PATH}/shared/files/translations"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Remove existing public files folder for symlink creation.${COLOR_NC}"
$SSH $SSH_ARGS "rm -rf ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/web/sites/default/files"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create symlink for public files.${COLOR_NC}"
$SSH $SSH_ARGS "ln -s ../../../../../shared/files ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/web/sites/default/files"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create shared private files folder in case it is the first deployment.${COLOR_NC}"
$SSH $SSH_ARGS "mkdir -p ${DEPLOYMENT_PATH}/shared/private"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Remove existing private files folder for symlink creation.${COLOR_NC}"
$SSH $SSH_ARGS "rm -rf ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/private"

echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create symlink for private files.${COLOR_NC}"
$SSH $SSH_ARGS "ln -s ../../shared/private ${DEPLOYMENT_PATH}/releases/${BUILD_VERSION}/private"
