#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh
NB_TO_KEEP=5

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

LS_RESULT=$($SSH $SSH_ARGS "ls -t ${DEPLOYMENT_PATH}/releases")
RELEASES=($LS_RESULT)

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Clean up releases on server.${COLOR_NC}"
echo "Number of releases on server: ${#RELEASES[@]}"

for RELEASE in "${RELEASES[@]}"
do
  echo "Release: ${RELEASE}"
done

echo "Number of releases to keep: ${NB_TO_KEEP}"

if [ ${#RELEASES[@]} -gt ${NB_TO_KEEP} ]; then
  NB_TO_REMOVE=$(expr ${#RELEASES[@]} - $NB_TO_KEEP)
  echo "Number of releases to remove: ${NB_TO_REMOVE}"

  RELEASES_TO_REMOVE=("${RELEASES[@]: -${NB_TO_REMOVE}}")
  for RELEASE_TO_REMOVE in "${RELEASES_TO_REMOVE[@]}"
  do
    echo "Deleting release: ${RELEASE_TO_REMOVE}"

    $SSH $SSH_ARGS "chmod 0775 -R ${DEPLOYMENT_PATH}/releases/${RELEASE_TO_REMOVE}/web/sites/default"
    $SSH $SSH_ARGS "rm -fr ${DEPLOYMENT_PATH}/releases/${RELEASE_TO_REMOVE}"
  done
else
  echo "Nothing to remove"
  exit 0;
fi
