#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh
RETENTION_TIME=180 #In days, simply assuming a day = 24 hours

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

LS_RESULT=$($SSH $SSH_ARGS "ls -t ${DEPLOYMENT_PATH}/backups")
BACKUPS=($LS_RESULT)

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Clean up backups on server.${COLOR_NC}"
echo "Number of backups on server: ${#BACKUPS[@]}"
echo "Retention of backups: ${RETENTION_TIME} days"

TIMESTAMP=$(date +%s)

for BACKUP in "${BACKUPS[@]}"
do
  BACKUP_MTIME=$($SSH $SSH_ARGS "stat -c %Y ${DEPLOYMENT_PATH}/backups/${BACKUP}")
  BACKUP_AGE=$(( (${TIMESTAMP} - ${BACKUP_MTIME}) / 86400 ))

  echo "Backup: ${BACKUP} => ${BACKUP_AGE} day(s)"

  if [ ${BACKUP_AGE} -gt ${RETENTION_TIME} ]; then
    echo "Removing"
    $SSH $SSH_ARGS "rm -f ${DEPLOYMENT_PATH}/backups/${BACKUP}"
  fi
done
