#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

if $SSH $SSH_ARGS "test -e ${DEPLOYMENT_PATH}/current"; then
  # Perform Drupal update with drush
  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Launch database updates.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH updatedb --no-cache-clear -y"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Launch database updates a second time. Just in case...${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH updatedb --no-cache-clear -y"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Clear cache to be sure cache are cleared even if there is no update.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH cache:rebuild"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Import configuration.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH config:import -y"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Import configuration a second time. In case the config of modules altering configuration import has changed.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH config:import -y"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Update translations status.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH locale:check"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Update translations.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH locale:update"

  #echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Import custom translations.${COLOR_NC}"
  # Expected translation file pattern is "XXX.[langcode].po".
  #for TRANSLATION_FILE_PATH in $($SSH $SSH_ARGS "find ${DEPLOYMENT_PATH}/sites/${!FOLDER_NAME}/current/app/translations/custom/*.po -type f")
  #do
  #  FILE_NAME=$(basename "${TRANSLATION_FILE_PATH}")
  #  LANGCODE=$(echo "${FILE_NAME}" | cut -d'.' -f2)
  #  $SSH $SSH_ARGS "$DRUSH locale:import \
  #    ${LANGCODE} \
  #    ${TRANSLATION_FILE_PATH} \
  #    --type=not-customized \
  #    --override=all"
  #done

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Run deploy hooks.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH deploy:hook -y"

  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Flush caches to be clean.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH cache:rebuild"
fi
