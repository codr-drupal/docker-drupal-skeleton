#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

if $SSH $SSH_ARGS "test -e ${DEPLOYMENT_PATH}/current"; then
  echo -e "${COLOR_LIGHT_GREEN}${SERVER_HOST}: Disable maintenance mode.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH state:set system.maintenance_mode 0"
fi
