#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

# Do the actual release, moving current symlink to new version
if $SSH $SSH_ARGS "test -e ${DEPLOYMENT_PATH}/current"; then
  echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Remove existing symlink.${COLOR_NC}"
  $SSH $SSH_ARGS "unlink ${DEPLOYMENT_PATH}/current"
fi

echo -e "${COLOR_LIGHT_GREEN}${DRUPAL_SITE}: Create symlink to new release.${COLOR_NC}"
$SSH $SSH_ARGS "ln -s ./releases/${BUILD_VERSION} ${DEPLOYMENT_PATH}/current"
