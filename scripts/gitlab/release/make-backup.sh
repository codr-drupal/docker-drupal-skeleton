#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Healthcheck
if ! eval ${HEALTHCHECK}; then
  echo -e "${COLOR_LIGHT_RED}Server ${SERVER_HOST}: Impossible to ping the server.${COLOR_NC}"
  exit 1
fi

# Perform DB Backup
echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Create backup folder.${COLOR_NC}"
$SSH $SSH_ARGS "mkdir -p ${DEPLOYMENT_PATH}/backups"

# Test if the "current" symlink exists for this website.
if $SSH $SSH_ARGS "test -e ${DEPLOYMENT_PATH}/current"; then
  echo -e "${COLOR_LIGHT_GREEN}Server ${SERVER_HOST}: Database backup.${COLOR_NC}"
  $SSH $SSH_ARGS "$DRUSH sql:dump \
    --result-file=${DEPLOYMENT_PATH}/backups/${CURRENT_DATE}-${APP_ENV}.sql \
    --gzip"
fi
