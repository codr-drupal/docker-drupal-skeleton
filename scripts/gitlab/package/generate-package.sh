#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Remove unnecessary file.
FILES_EXCLUDED_FROM_PACKAGE=(
  assets
  build
  config/mariadb
  config/solr
  docker
  docs
  dump
  patches
  scripts
  web/.csslintrc
  web/.eslintignore
  web/.eslintrc.json
  web/.ht.router.php
  web/sites/development.services.yml
  .dockerignore
  .editorconfig
  .env.dist
  .git
  .gitattributes
  .gitignore
  .gitlab-ci.yml
  docker.mk
  docker-compose.override.dist.yml
  docker-compose.yml
  drupal.mk
  node.mk
  phpstan.neon
  Makefile
  README.md
)

# Delete some files
for FILE_EXCLUDED_FROM_PACKAGE in "${FILES_EXCLUDED_FROM_PACKAGE[@]}"
do
  echo -e "Delete file: ${BUILD_PATH}/${FILE_EXCLUDED_FROM_PACKAGE}"
  rm -rf "${BUILD_PATH}/${FILE_EXCLUDED_FROM_PACKAGE}"
done

# Generate final build (dated based)
FINAL_BUILD_VERSION="${CI_PROJECT_NAME}"-"${BUILD_TYPE}"-"${CURRENT_DATE}"-${BUILD_VERSION}

# Export build_version
echo -e "${COLOR_LIGHT_GREEN}Export version name for usage in CI${COLOR_NC}"
echo "${FINAL_BUILD_VERSION}" > "${ARTIFACT_PATH}"/.BUILD_VERSION

# Rename build dir
echo -e "${COLOR_LIGHT_GREEN}Rename package folder${COLOR_NC}"
mv "${BUILD_PATH}" "${FINAL_BUILD_VERSION}"

#Create archive
echo -e "${COLOR_LIGHT_GREEN}Create archive${COLOR_NC}"
tar -cz -f "${FINAL_BUILD_VERSION}.tar.gz" "${FINAL_BUILD_VERSION}"

#Remove archived dir
echo -e "${COLOR_LIGHT_GREEN}Remove build directory (Only keep archive and scripts)${COLOR_NC}"
sudo rm -fr "${FINAL_BUILD_VERSION}"
