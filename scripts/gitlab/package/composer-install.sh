#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname "${BASH_SOURCE[0]}"))/init.sh

# Perform composer install for prod
composer install \
  --ignore-platform-reqs \
  --no-interaction \
  --no-progress \
  --no-dev \
  --optimize-autoloader \
  --working-dir="${BUILD_PATH}"