#!/usr/bin/env bash

ARTIFACT_PATH="$(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))"
BUILD_PATH=${ARTIFACT_PATH}/build

# Check that all required parameters are present.
if [ -z ${NO_BUILD} ] && [ ! -f "${ARTIFACT_PATH}/.BUILD_VERSION" ]; then
  echo "Missing .BUILD_VERSION file."
  exit 1
fi

BUILD_VERSION=$(cat ${ARTIFACT_PATH}/.BUILD_VERSION)
CURRENT_DATE=$(date "+%Y%m%d%H%M%S")

#HEALTHCHECK='ping -c 1 ${SERVER_HOST}'
HEALTHCHECK='nc -z -w2 ${SERVER_HOST} ${SERVER_PORT}'

SSH='ssh -o StrictHostKeyChecking=no -o ServerAliveInterval=5'
SSH_ARGS="${SSH_USER}@${SERVER_HOST} -p ${SERVER_PORT}"

DRUSH="${DEPLOYMENT_PATH}/current/vendor/bin/drush @default.alias"

# For kairn
# DRUSH="/usr/local/php-8.2/bin/php ${DEPLOYMENT_PATH}/current/vendor/bin/drush @default.kairn"

### Colors.
COLOR_NC='\033[0m' # No Color.
COLOR_BLACK='\033[0;30m'
COLOR_DARK_GRAY='\033[1;30m'
COLOR_RED='\033[0;31m'
COLOR_LIGHT_RED='\033[1;31m'
COLOR_GREEN='\033[0;32m'
COLOR_LIGHT_GREEN='\033[1;32m'
COLOR_BROWN_ORANGE='\033[0;33m'
COLOR_YELLOW='\033[1;33m'
COLOR_BLUE='\033[0;34m'
COLOR_LIGHT_BLUE='\033[1;34m'
COLOR_PURPLE='\033[0;35m'
COLOR_LIGHT_PURPLE='\033[1;35m'
COLOR_CYAN='\033[0;36m'
COLOR_LIGHT_CYAN='\033[1;36m'
COLOR_LIGHT_GRAY='\033[0;37m'
COLOR_WHITE='\033[1;37m'
