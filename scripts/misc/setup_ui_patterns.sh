#!/usr/bin/env bash

# Init some vars
. $(dirname $(dirname $(dirname "${BASH_SOURCE[0]}")))/.env

# Modules to require via composer
MODULES_TO_REQUIRE=(
  drupal/block_field
  drupal/blockgroup
  drupal/computed_field
  drupal/ds
  drupal/entity_usage
  drupal/fences
  drupal/layout_paragraphs
  drupal/paragraph_view_mode
  drupal/paragraphs
  drupal/style_options
  drupal/ui_patterns
  drupal/viewsreference
)

# Modules to install via drush
MODULES_TO_INSTALL=(
  block_field
  blockgroup
  computed_field
  ds
  entity_usage
  fences
  layout_discovery
  layout_paragraphs
  paragraphs
  paragraph_view_mode
  style_options
  ui_patterns
  ui_patterns_layouts
  ui_patterns_library
  ui_patterns_views
  viewsreference
)

echo "Preparing project for 'ui_patterns' stack"

# Composer
echo "Requiring ${MODULES_TO_REQUIRE[@]}"
composer require ${MODULES_TO_REQUIRE[@]}

# Drush
echo "Installing ${MODULES_TO_INSTALL[@]}"
drush @default.alias en ${MODULES_TO_INSTALL[@]} -y

# Custom module
echo "Generating and installing custom patterns module"
drush @default.alias generate module -y \
  --destination=modules/custom \
  --answer="${PROJECT_NAME} Patterns" \
  --answer="${PROJECT_NAME,,}_patterns" \
  --answer="Pattern module for ${PROJECT_NAME,,}" \
  --answer=Custom \
  --answer="" \
  --answer=yes \
  --answer=no \
  --answer=no

drush @default.alias en "${PROJECT_NAME,,}_patterns" -y

drush @default.alias cex -y
