# 🐳 Docker Drupal Skeleton 10.2.x

This project provides a stack based on [wodby/docker4drupal](https://github.com/wodby/docker4drupal)

# 🥞 About this stack

* [More about this stack](docs/skeleton/stack/00-intro.md)
* [Environment management](docs/skeleton/stack/01-env.md)
* [`make` commands](docs/skeleton/stack/02-make.md)

# 🏁 Start a new project

* [Prerequisites](docs/skeleton/project/00-prerequisites.md)
* [Start a project](docs/skeleton/project/01-new-project.md)
* [CI/CD](docs/skeleton/project/02-cicd.md)

# 🚀 Launch an existing project

* [Launch project](docs/skeleton/project/03-existing-project.md)

# 🚀 Configure your IDE

* [PHP Storm](docs/skeleton/ide/00-phpstorm.md)
* [VS Code](docs/skeleton/ide/01-vscode.md)

# 🧨 Troubleshooting

* [Troubleshooting](docs/skeleton/stack/03-troubleshooting.md)
